<script src="js/jquery-3.6.0.min.js"></script>
<script src="js/main.js"></script>
<link href="bootstrap/bootstrap.min.css" rel="stylesheet">

<table class="matriz-datatables table table-striped table-bordered" id="id_matriz">
    <thead>
        <tr>
            <th>Id </th>
            <th>Nombre </th>
            <th>Apellido </th>
            <th>Usuario</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="id">1</td>
            <td>Sergio</td>
            <td>Avila</td>
            <td>savila</td>
            <td>
                <button type="button" class="get-id btn btn-primary btn-sm">
                    <span class="fas fa-redo-alt" title="Obtener ID">Get</span>
                </button>

            </td>
        </tr>
        <tr>
            <td class="id">2</td>
            <td>Ruben</td>
            <td>Peuchele</td>
            <td>rpeuchele</td>
            <td>
                <button type="button" class="get-id btn btn-primary btn-sm">
                    <span class="fas fa-redo-alt" title="Obtener ID">Get</span>
                </button>

            </td>
        </tr>
        <tr>
            <td class="id">3</td>
            <td>Taras</td>
            <td>Bulba</td>
            <td>tbulba</td>
            <td>
                <button type="button" class="get-id btn btn-primary btn-sm">
                    <span class="fas fa-redo-alt" title="Obtener ID">Get</span>
                </button>

            </td>
        </tr>
        <tr>
            <td class="id">4</td>
            <td>Iron</td>
            <td>Man</td>
            <td>iman</td>
            <td>
                <button type="button" class="get-id btn btn-primary btn-sm">
                    <span class="fas fa-redo-alt" title="Obtener ID">Get</span>
                </button>

            </td>
        </tr>
    </tbody>
</table>